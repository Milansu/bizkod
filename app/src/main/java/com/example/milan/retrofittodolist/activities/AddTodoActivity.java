package com.example.milan.retrofittodolist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.milan.retrofittodolist.R;
import com.example.milan.retrofittodolist.api.ApiClient;
import com.example.milan.retrofittodolist.api.JsonPlaceHolderApi;
import com.example.milan.retrofittodolist.model.Post;
import com.example.milan.retrofittodolist.services.SharedPref;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTodoActivity extends AppCompatActivity {
    EditText ettodo;
    Button btnTodo;
    private String todo;
    Integer id, customer_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ettodo = findViewById(R.id.etTodo);
        btnTodo = findViewById(R.id.btnTodo);

        btnTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPost();
            }
        });


    }

    private void createPost() {
        todo = ettodo.getText().toString();
        customer_id = Integer.valueOf(SharedPref.getInstance(this).LoggedInUserId());
        Post post = new Post(
                customer_id,
                todo
        );

        JsonPlaceHolderApi jsonPlaceHolderApi = ApiClient.getClient().create(JsonPlaceHolderApi.class);
        Call<Post> call = jsonPlaceHolderApi.createPost(post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(@NonNull Call<Post> call, @NonNull Response<Post> response) {
                String todo = response.body().getMessage();
                if (todo.equals("post created"))
                    Toast.makeText(AddTodoActivity.this, todo, Toast.LENGTH_SHORT).show();
                loadDashboard();
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Toast.makeText(AddTodoActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.d("gdejeproblem", t.toString());
            }
        });

    }

    private void loadDashboard() {
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        finish();

    }
}
