package com.example.milan.retrofittodolist.activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.milan.retrofittodolist.R;
import com.example.milan.retrofittodolist.services.SharedPref;

public class ProfileActivity extends AppCompatActivity {
    TextView username;
    Button btnLogout, btnTodo, btnAddTodo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        username = findViewById(R.id.username);
        btnLogout = findViewById(R.id.btnLogout);
        btnTodo = findViewById(R.id.btnTodo);
        btnAddTodo = findViewById(R.id.btnAddTodo);

//check if user is logged in
        if (!SharedPref.getInstance(this).isLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

//getting logged in user name
        String loggedUsename = SharedPref.getInstance(this).LoggedInUser();
        username.setText("Username : "+loggedUsename);

        btnTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
                finish();
            }
        });

//logging out
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SharedPref.getInstance(getApplicationContext()).logout();
            }
        });

        btnAddTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),AddTodoActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

}

