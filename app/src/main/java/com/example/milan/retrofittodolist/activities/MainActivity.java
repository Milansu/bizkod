package com.example.milan.retrofittodolist.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.milan.retrofittodolist.R;
import com.example.milan.retrofittodolist.adapters.TodoAdapter;
import com.example.milan.retrofittodolist.api.ApiClient;
import com.example.milan.retrofittodolist.api.JsonPlaceHolderApi;
import com.example.milan.retrofittodolist.model.Post;
import com.example.milan.retrofittodolist.model.User;
import com.example.milan.retrofittodolist.services.SharedPref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    TodoAdapter todoAdapter;
    ListView mListView;
    String loggedId = SharedPref.getInstance(this).LoggedInUserId();
    Integer id = Integer.valueOf(loggedId);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = findViewById(R.id.mListView);

        JsonPlaceHolderApi jsonPlaceHolderApi = ApiClient.getClient().create(JsonPlaceHolderApi.class);

/* TODO: Ubaciti Sessionhandler, iz Login (Register acitivityja kad uradis ovo prethodno jer ce se iz registrovanje prebaciti odma
   TODO: u MainActivity) ubaciti id preko SharedPreferenca u ovaj activity
   TODO: Napraviti CreateToDoTask i brisanje

 */

        Call<ArrayList<Post>> call = jsonPlaceHolderApi.getPosts(loggedId);

        call.enqueue(new Callback<ArrayList<Post>>() {
            @Override
            public void onResponse(Call<ArrayList<Post>> call, Response<ArrayList<Post>> response) {

                ArrayList<Post> posts = response.body();
                todoAdapter = new TodoAdapter(getApplicationContext(), posts);
                mListView.setAdapter(todoAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<Post>> call, Throwable t) {
            }
        });
    }

}