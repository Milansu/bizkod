package com.example.milan.retrofittodolist.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.milan.retrofittodolist.R;
import com.example.milan.retrofittodolist.api.ApiClient;
import com.example.milan.retrofittodolist.api.JsonPlaceHolderApi;
import com.example.milan.retrofittodolist.model.Post;
import com.example.milan.retrofittodolist.model.User;
import com.example.milan.retrofittodolist.services.SharedPref;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoAdapter extends ArrayAdapter<Post> {

    private Context context;
    private ArrayList<Post> postArrayList;
    ImageButton delete;

    public TodoAdapter(Context context, ArrayList<Post> postArrayList) {
        super(context, 0, postArrayList);
        this.context = context;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Post post = getItem(position);


        if (view == null) {

            view = LayoutInflater.from(getContext()).inflate(R.layout.listview_adapter, parent, false);
        }

        TextView todo = view.findViewById(R.id.text);
        final TextView tvId = view.findViewById(R.id.description);
        delete = view.findViewById(R.id.delete);


        todo.setText(post.getTodo());
        tvId.setText(String.valueOf(post.getId()));
        final int id = post.getId();

        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                JsonPlaceHolderApi api = ApiClient.getClient().create(JsonPlaceHolderApi.class);
                Call<Post> delete = api.deletePost(id);

                delete.enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(Call<Post> call, Response<Post> response) {
                        String isSuccess = response.body().getMessage();
                        if (isSuccess.equals("post deleted")) {
                            Toast.makeText(getContext(), "Deleted " + id + " post.", Toast.LENGTH_SHORT).show();
//                            postArrayList.remove(position);
//                            notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<Post> call, Throwable t) {

                    }
                });


            }
        });


        return view;
    }
}
