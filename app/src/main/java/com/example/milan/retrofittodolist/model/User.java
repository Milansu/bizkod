package com.example.milan.retrofittodolist.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class User {

    @SerializedName("id")
    private Integer id;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("surname")
    private String surname;
    @SerializedName("contact")
    private String contact;
    @SerializedName("password")
    private String password;
    @SerializedName("message")
    private String message;
    Date sessionExpiryDate;

    public String getMessage() {
        return message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User() {

    }
    public User(Integer id, String email, String name, String surname, String contact, String password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.contact = contact;
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getContact() {
        return contact;
    }

    public void setSessionExpiryDate(Date sessionExpiryDate) {
        this.sessionExpiryDate = sessionExpiryDate;
    }

    public Date getSessionExpiryDate() {
        return sessionExpiryDate;
    }
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
