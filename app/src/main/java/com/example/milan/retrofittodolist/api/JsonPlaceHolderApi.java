package com.example.milan.retrofittodolist.api;

import com.example.milan.retrofittodolist.model.Post;
import com.example.milan.retrofittodolist.model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @GET("todolist.php")
    Call<ArrayList<Post>> getPosts(@Query("id") String id);

    @POST("register.php")
    Call<User> register(@Body User user);

    @POST("login.php")
    Call<User> login(@Body User user);

    @POST("create_todo.php")
    Call<Post> createPost(@Body Post post);

    @DELETE("delete.php")
    Call<Post> deletePost(@Query("id") int id);

}