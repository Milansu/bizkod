package com.example.milan.retrofittodolist.model;


import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("id")
    private int id;
    @SerializedName("todo")
    private String todo;
    @SerializedName("message")
    private String message;

    public Post(Integer id, String todo) {
        this.id = id;
        this.todo = todo;
    }

    public int getId() {
        return id;
    }

    public String getTodo() {
        return todo;
    }

    public String getMessage() {
        return message;
    }
}
