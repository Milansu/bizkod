package com.example.milan.retrofittodolist.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.milan.retrofittodolist.R;
import com.example.milan.retrofittodolist.services.SharedPref;
import com.example.milan.retrofittodolist.api.ApiClient;
import com.example.milan.retrofittodolist.api.JsonPlaceHolderApi;
import com.example.milan.retrofittodolist.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {
    EditText email_input, password_input;
    TextView register;
    Button btnLogin;
    Vibrator v;

    //change this to match your url
    final String loginURL = "http://chocolatefor.me/ToDo/api/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email_input = findViewById(R.id.userName);
        password_input = findViewById(R.id.loginPassword);
        register = findViewById(R.id.register);
        btnLogin = findViewById(R.id.btnLogin);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateUserData();
            }
        });

        //when someone clicks on login
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void validateUserData() {

        //first getting the values
        final String email = email_input.getText().toString();
        final String password = password_input.getText().toString();

        //checking if username is empty
        if (TextUtils.isEmpty(email)) {
            email_input.setError("Please enter your email");
            email_input.requestFocus();
            // Vibrate for 100 milliseconds
            v.vibrate(100);
            btnLogin.setEnabled(true);
            return;
        }
        //checking if password is empty
        if (TextUtils.isEmpty(password)) {
            password_input.setError("Please enter your password");
            password_input.requestFocus();
            //Vibrate for 100 milliseconds
            v.vibrate(100);
            btnLogin.setEnabled(true);
            return;
        }


        //Login User if everything is fine
        loginUser();


    }

    private void loadDashboard() {
        Intent i = new Intent(getApplicationContext(),ProfileActivity.class);
        startActivity(i);
        finish();

    }
    private void loginUser() {

        //making api call
        User user = new User(
                email_input.getText().toString(),
                password_input.getText().toString()
        );
        JsonPlaceHolderApi api = ApiClient.getClient().create(JsonPlaceHolderApi.class);
        Call<User> login = api.login(user);

        login.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {


                //get username
                String msg = response.body().getMessage();

                if(msg.equals("uspesno ste se ulogovali")) {
                    //storing the user in shared preferences
                    String userName= response.body().getName();
                    String userId= response.body().getId().toString();

                    SharedPref.getInstance(LoginActivity.this).storeUserName(userName);
                    SharedPref.getInstance(LoginActivity.this).storeId(userId);
                    loadDashboard();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

}

